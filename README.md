# OpenML dataset: Measles-Immunization-Rates-in-US-Schools

https://www.openml.org/d/43638

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data set contains measles vaccination rate data for 46,412 schools in 32 states across the US. 
Content
Vaccination rates are for the 2017-201818 school year for the following states: 

Colorado
Connecticut
Minnesota
Montana
New Jersey
New York
North Dakota
Pennsylvania
South Dakota
Utah
Washington

Rates for other states are for the time period 2018-2019. 
The data was compiled by The Wall Street Journal.
Acknowledgements
The data was originally compiled by The Wall Street Journal, and then downloaded and wrangled by the TidyTuesday community. The R code used for wrangling can be accessed here.
Inspiration
Please remember that you are welcome to explore beyond the provided data set, but the data is provided as a "toy" data set to practice techniques on. The data may require additional cleaning and wrangling!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43638) of an [OpenML dataset](https://www.openml.org/d/43638). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43638/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43638/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43638/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

